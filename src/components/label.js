import React, {Component} from 'react';

const Label = ({text}) => (
  <h3 className="ui header">{text}</h3>
);

Label.propTypes = {
  text: React.PropTypes.string.isRequired
};

export default Label;
