import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';

class Dropdown extends Component {

  componentDidMount() {
    const $dropdown = $(ReactDOM.findDOMNode(this.refs.dropdown));
    this.dropdown = $.fn.dropdown.bind($dropdown);

    this.dropdown('set selected', this.props.value)
        .dropdown({onChange: this.props.onChange});
  }

  render() {
    return (
      <div className="ui selection dropdown" ref="dropdown">
        <div className="default text">Selection Dropdown</div>
        <i className="dropdown icon"></i>
        <div className="menu">
          {this.props.options.map((option, index) => (
            <div className="item" key={index} data-value={index}>{option}</div>
          ))}
        </div>
      </div>
    );
  }
}

Dropdown.propTypes = {
  options:  React.PropTypes.array.isRequired,
  value: React.PropTypes.string.isRequired,
  onChange: React.PropTypes.func.isRequired
};

export default Dropdown;
