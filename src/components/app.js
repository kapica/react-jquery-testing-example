import React, { Component } from 'react';
import Dropdown from './dropdown';
import Label from './label';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      options: ['Foo', 'Bar', 'Baz'],
      value: 'Foo'
    };

    this.handleDropdownChange = this.handleDropdownChange.bind(this);
  }

  handleDropdownChange(_index, value) {
    this.setState({value});
  }

  render() {
    return (
      <div>
        <Label text={this.state.value} />
        <Dropdown {...this.state} onChange={this.handleDropdownChange} />
      </div>
    );
  }
}
