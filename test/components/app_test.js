import { renderComponent , expect } from '../test_helper';
import App from '../../src/components/app';

describe('App' , () => {
  let component;

  beforeEach(() => {
    component = renderComponent(App);
  });

  it('renders something', () => {
    expect(component).to.exist;
  });

  it('renders foo by default', () => {
    const labelValue = component.find('.label-component').text();
    expect(labelValue).to.equal('Foo');
  });

  it('renders Bar when it is selected', () => {
    const dropdown = component.find('.dropdown-component select');
    dropdown.simulate('change', 1);
    const labelValue = component.find('.label-component').text();
    expect(labelValue).to.equal('Bar');
  });

  // Test helper needs a fix
  it('renders Bar and then Foo when changed back', () => {
    const dropdown = component.find('.dropdown-component select');
    const label = component.find('.label-component');
    dropdown.simulate('change', 1);
    expect(label.text()).to.equal('Bar');
    dropdown.simulate('change', 0);
    expect(label.text()).to.equal('Foo');
  });
});
